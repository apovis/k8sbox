#!/usr/bin/env bash

# last modified 2024.04.24
# create a delpoyable secret for k8s
# from your private pki

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

# create a temp dir
export TMP_KEY_DIR="$(mktemp -qd)"

echo "full path to your CA cert-file:"
read CA_CERT
export CA_CERT

export CA_CERT_B64="$(cat ${CA_CERT} | base64 -w0)"

echo "full path to your CA key-file:"
read CA_KEY
export CA_KEY
export CA_KEY_NO_PWD="${TMP_KEY_DIR}/ca_nopwd.key"

echo "what is your CA's name:"
read CA_NAME_ORIG
export CA_NAME="$(echo ${CA_NAME_ORIG} | tr [:upper:] [:lower:])"

echo "in which namespace should the secret be?"
read NAMESPACE_SECRET
export NAMESPACE_SECRET

echo "in which namespace should the ca issuer be?"
read NAMESPACE_CA_ISSUER
export NAMESPACE_CA_ISSUER

export TEMPLATES_DIR="$(pwd)/templates"
export CA_SECRET_YAML="${TEMPLATES_DIR}/ca_secret.yaml"
export TEMP_CA_SECRET_YAML="${TMP_KEY_DIR}/ca_secret.yaml"
export CA_ISSUER_YAML="${TEMPLATES_DIR}/ca_issuer.yaml"
export TEMP_CA_ISSUER_YAML="${TMP_KEY_DIR}/ca_issuer.yaml"


# test for pwd on key-file and remove it
if ! [[ -f ${CA_KEY} ]] 
  then
    echo "file ${CA_KEY} does not exist"
    exit 1

  elif [[ $(file ${CA_KEY} | grep "with password") ]] && [[ $? = 0 ]]
  then
    echo "removing password from file"
    openssl rsa -in ${CA_KEY} -out ${CA_KEY_NO_PWD}
    export CA_KEY_B64="$(cat ${CA_KEY_NO_PWD} | base64 -w0)"

  elif [[ $(file ${CA_KEY} | grep "no password") ]] && [[ $? = 0 ]]
  then
    echo "file does not have a pwd"
    export CA_KEY_B64="$(cat ${CA_KEY} | base64 -w0)"

  else
    echo "something is wrong here"
    exit 1

fi

# replace vars in ca_secret.yaml
cp ${CA_SECRET_YAML} ${TEMP_CA_SECRET_YAML}
sed -i "s|<CA_NAME>|${CA_NAME}|g" "${TEMP_CA_SECRET_YAML}"
sed -i "s|<CA_CERT_B64>|${CA_CERT_B64}|g" "${TEMP_CA_SECRET_YAML}"
sed -i "s|<CA_KEY_B64>|${CA_KEY_B64}|g" "${TEMP_CA_SECRET_YAML}"
sed -i "s|<NAMESPACE_SECRET>|${NAMESPACE_SECRET}|g" "${TEMP_CA_SECRET_YAML}"

# replace CA_NAME in ca_issuer.yaml
cp ${CA_ISSUER_YAML} ${TEMP_CA_ISSUER_YAML}
sed -i "s|<CA_NAME>|${CA_NAME}|g" "${TEMP_CA_ISSUER_YAML}"
sed -i "s|<NAMESPACE_CA_ISSUER>|${NAMESPACE_CA_ISSUER}|g" "${TEMP_CA_ISSUER_YAML}"

# list files
echo "done creating files:"
ls -lah ${TMP_KEY_DIR}/

echo "do not forget to remove ${TMP_KEY_DIR}, after deploying the files :-) "

exit 0