#!/usr/bin/env bash

# last modified 2024.04.24
# create a delpoyable secret for k8s
# from your private/public certs

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

# create a temp dir
export TMP_KEY_DIR="$(mktemp -qd)"

echo "full path to your public cert-file:"
read PUB_CERT
export PUB_CERT
export PUB_CERT_B64="$(cat ${PUB_CERT} | base64 -w0)"

echo "full path to your private key-file:"
read PRIV_KEY
export PRIV_KEY
export PRIV_KEY_NO_PWD="${TMP_KEY_DIR}/priv_nopwd.key"

echo "in which namespace should the secret be?"
read NAMESPACE_SECRET
export NAMESPACE_SECRET

echo "enter a name for your cert please:"
read CERT_NAME_ORIG
export CERT_NAME="$(echo ${CERT_NAME_ORIG} | tr [:upper:] [:lower:])"

export TEMPLATES_DIR="$(pwd)/templates"
export CERT_SECRET_YAML="${TEMPLATES_DIR}/cert_secret.yaml"
export TEMP_CERT_SECRET_YAML="${TMP_KEY_DIR}/cert_secret.yaml"


# test for pwd on key-file and remove it
if ! [[ -f ${PRIV_KEY} ]] 
  then
    echo "file ${PRIV_KEY} does not exist"
    exit 1

  elif [[ $(file ${PRIV_KEY} | grep "with password") ]] && [[ $? = 0 ]]
  then
    echo "removing password from file"
    openssl rsa -in ${PRIV_KEY} -out ${PRIV_KEY_NO_PWD}
    export PRIV_KEY_B64="$(cat ${PRIV_KEY_NO_PWD} | base64 -w0)"

  elif [[ $(file ${PRIV_KEY} | grep "no password") ]] && [[ $? = 0 ]]
  then
    echo "file does not have a pwd"
    export PRIV_KEY_B64="$(cat ${PRIV_KEY} | base64 -w0)"

  else
    echo "something is wrong here"
    exit 1

fi

# replace vars in ca_secret.yaml
cp ${CERT_SECRET_YAML} ${TEMP_CERT_SECRET_YAML}
sed -i "s|<CERT_NAME>|${CERT_NAME}|g" "${TEMP_CERT_SECRET_YAML}"
sed -i "s|<PUB_CERT_B64>|${PUB_CERT_B64}|g" "${TEMP_CERT_SECRET_YAML}"
sed -i "s|<PRIV_KEY_B64>|${PRIV_KEY_B64}|g" "${TEMP_CERT_SECRET_YAML}"
sed -i "s|<NAMESPACE_SECRET>|${NAMESPACE_SECRET}|g" "${TEMP_CERT_SECRET_YAML}"

# list files
echo "done creating files:"
ls -lah ${TMP_KEY_DIR}/

echo "do not forget to remove ${TMP_KEY_DIR}, after deploying the files :-) "

exit 0